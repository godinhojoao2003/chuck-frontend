## ChuckApp frontend (fullstack, api at other repo)

- During this project, I challenged myself: "How quickly can you develop something with a basic structure that can scale?"

  - I've developed similar projects before as a developer, but this time I was trying to be a bit faster
  - I've also used this project to recap some content in my mind during the development.

- How to run (linux and mac):

1. You need to install nodejs, nvm and npm.
2. `nvm install v20.14.0`
3. `nvm use`
4. `npm run dev`

- How to run lint after changing something

1. `npm run lint` or `npm run lint-fix`
