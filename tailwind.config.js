import withMT from "@material-tailwind/react/utils/withMT";

/** @type {import('tailwindcss').Config} */
export default withMT({
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
    // './src/pages/**/*.{ts,jsx,tsx}',
    // './src/components/**/*.{ts,jsx,tsx}',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
});

