
export enum JokeCategory {
  Uncategorized = "uncategorized",
  Animal = "animal",
  Career = "career",
  Dev = "dev",
  Food = "food",
  Music = "music",
  Money = "money",
  Movie = "movie",
  Travel = "travel"
}

export interface Joke {
  category: JokeCategory;
  created_at: string;
  icon_url: string;
  id: string;
  updated_at: string;
  url: string;
  value: string;
}