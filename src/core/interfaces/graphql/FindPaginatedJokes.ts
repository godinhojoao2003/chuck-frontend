import { Joke } from "../types/Joke";

export interface FindPaginatedJokesInput {
  input: {
    limit: number;
    offset: number;
    searchText: string;
  }
}

export interface FindPaginatedJokesResponse {
  findPaginatedJokes: {
    items: Joke[];
    totalCount: number;
  };
}