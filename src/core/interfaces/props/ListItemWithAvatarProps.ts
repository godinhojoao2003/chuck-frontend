import { Joke } from "../types/Joke";

export interface ListItemWithAvatarProps {
  joke: Joke;
  handleClick: (joke: Joke) => void;
}