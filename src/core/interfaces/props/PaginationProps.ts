export interface PaginationProps {
  onPageChange: (pageNumber: number) => void;
  lastPage: number;
  page: number;
  setPage: (pageNumber: number) => void;
}
