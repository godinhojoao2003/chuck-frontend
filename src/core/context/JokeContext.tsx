import React, { createContext, useState, ReactNode } from "react";
import { Joke } from "../interfaces/types/Joke";

export interface JokeContextValue {
  joke: Joke | null;
  setJoke: (joke: Joke | null) => void;
  allJokes: Joke[] | undefined;
  setAllJokes: (jokes: Joke[] | undefined) => void;
  visibleJokes: Joke[] | undefined;
  setVisibleJokes: (jokes: Joke[] | undefined) => void;
  totalJokes: number;
  setTotalJokes: (count: number) => void;
}

export const JokeContext = createContext<JokeContextValue | undefined>(
  undefined
);

export const JokeProvider: React.FC<{ children: ReactNode }> = ({
  children,
}) => {
  const [joke, setJoke] = useState<Joke | null>(null);
  const [allJokes, setAllJokes] = useState<Joke[] | undefined>();
  const [visibleJokes, setVisibleJokes] = useState<Joke[] | undefined>();
  const [totalJokes, setTotalJokes] = useState<number>(0);
  const jokeContextValue: JokeContextValue = {
    joke,
    setJoke,
    allJokes,
    setVisibleJokes,
    visibleJokes,
    setAllJokes,
    setTotalJokes,
    totalJokes,
  };
  return (
    <JokeContext.Provider value={jokeContextValue}>
      {children}
    </JokeContext.Provider>
  );
};
