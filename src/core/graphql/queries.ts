import { gql } from '@apollo/client';

export const FIND_PAGINATED_JOKES_QUERY = gql`
  query findPaginatedJokes($input: FindPaginatedJokesInput!) {
    findPaginatedJokes(input: $input) {
      items {
        id
        category
        created_at
        updated_at
        icon_url
        url
        value
      }
      totalCount
    }
  }
`;
