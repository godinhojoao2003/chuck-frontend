import { Card, List } from "@material-tailwind/react";
import { ListItemWithAvatar } from "../ListItemWithAvatar";
import { JokeContext, JokeContextValue } from "../../context/JokeContext";
import { useContext } from "react";

export function JokesList(): JSX.Element {
  const { visibleJokes, setJoke } =
    useContext(JokeContext) || ({} as JokeContextValue);

  if (!(visibleJokes && visibleJokes.length)) return <></>;
  return (
    <Card
      className="w-96"
      placeholder={undefined}
      onPointerEnterCapture={undefined}
      onPointerLeaveCapture={undefined}
    >
      <List
        placeholder={undefined}
        onPointerEnterCapture={undefined}
        onPointerLeaveCapture={undefined}
      >
        {visibleJokes.map((joke) => (
          <ListItemWithAvatar handleClick={setJoke} key={joke.id} joke={joke} />
        ))}
      </List>
    </Card>
  );
}
