import {
  ListItem,
  ListItemPrefix,
  Avatar,
  Typography,
} from "@material-tailwind/react";
import { ListItemWithAvatarProps } from "../../interfaces/props/ListItemWithAvatarProps";
import { getImageFileGivenCategory } from "../../shared/getImagePathGivenCategory";

export function ListItemWithAvatar({
  joke,
  handleClick,
}: ListItemWithAvatarProps): JSX.Element {
  return (
    <ListItem
      key={joke.id}
      placeholder={undefined}
      onPointerEnterCapture={undefined}
      onPointerLeaveCapture={undefined}
      onClick={() => handleClick(joke)}
    >
      <ListItemPrefix
        placeholder={undefined}
        onPointerEnterCapture={undefined}
        onPointerLeaveCapture={undefined}
      >
        <Avatar
          variant="circular"
          alt={joke.category}
          src={getImageFileGivenCategory(joke.category)}
          style={{ objectFit: "contain" }}
          placeholder={undefined}
          onPointerEnterCapture={undefined}
          onPointerLeaveCapture={undefined}
        />
      </ListItemPrefix>
      <div>
        <Typography
          variant="h6"
          color="blue-gray"
          placeholder={undefined}
          onPointerEnterCapture={undefined}
          onPointerLeaveCapture={undefined}
        >
          Chuck Norris Joke
        </Typography>
        <Typography
          variant="small"
          color="gray"
          className="font-normal"
          placeholder={undefined}
          onPointerEnterCapture={undefined}
          onPointerLeaveCapture={undefined}
        >
          {joke.value}
        </Typography>
        <div className="flex flex-col mt-1">
          <Typography
            variant="small"
            color="blue"
            className="mr-2 text-sm"
            placeholder={undefined}
            onPointerEnterCapture={undefined}
            onPointerLeaveCapture={undefined}
          >
            category: {joke.category}
          </Typography>
          <Typography
            variant="small"
            color="blue"
            className="mr-2 text-sm"
            placeholder={undefined}
            onPointerEnterCapture={undefined}
            onPointerLeaveCapture={undefined}
          >
            created at: {new Date(joke.created_at).toLocaleDateString()}
          </Typography>
        </div>
      </div>
    </ListItem>
  );
}
