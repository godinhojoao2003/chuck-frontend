import { useContext } from "react";
import {
  Button,
  Dialog,
  DialogHeader,
  DialogBody,
  DialogFooter,
} from "@material-tailwind/react";
import { JokeContext, JokeContextValue } from "../../context/JokeContext";

export function JokeDialog(): JSX.Element {
  const { setJoke, joke } = useContext(JokeContext) || ({} as JokeContextValue);
  if (!joke) return <></>;

  const handleOpen = (): void => setJoke(null);

  return (
    <Dialog
      open={joke !== null}
      size={"md"}
      handler={handleOpen}
      placeholder={undefined}
      onPointerEnterCapture={undefined}
      onPointerLeaveCapture={undefined}
    >
      <DialogHeader
        placeholder={undefined}
        onPointerEnterCapture={undefined}
        onPointerLeaveCapture={undefined}
      >
        Chuck Norris - Joke category: {joke?.category}
      </DialogHeader>
      <DialogBody
        placeholder={undefined}
        onPointerEnterCapture={undefined}
        onPointerLeaveCapture={undefined}
      >
        {joke?.value}
      </DialogBody>
      <DialogFooter
        placeholder={undefined}
        onPointerEnterCapture={undefined}
        onPointerLeaveCapture={undefined}
      >
        <Button
          variant="gradient"
          color="green"
          onClick={handleOpen}
          placeholder={undefined}
          onPointerEnterCapture={undefined}
          onPointerLeaveCapture={undefined}
        >
          <span>Close Joke</span>
        </Button>
      </DialogFooter>
    </Dialog>
  );
}
