import { useContext } from "react";
import { IconButton, Typography } from "@material-tailwind/react";
import { ArrowRightIcon, ArrowLeftIcon } from "@heroicons/react/24/outline";
import { PaginationProps } from "../../interfaces/props/PaginationProps";
import {
  LoadingContext,
  LoadingContextValue,
} from "../../context/LoadingContext";

export function Pagination({
  onPageChange,
  lastPage,
  page,
  setPage,
}: PaginationProps): JSX.Element {
  const { isLoading } =
    useContext(LoadingContext) || ({} as LoadingContextValue);

  const next = (): void => {
    if (page === lastPage) return;
    setPage(page + 1);
    onPageChange(page + 1);
  };

  const prev = (): void => {
    if (page === 1) return;
    setPage(page - 1);
    onPageChange(page - 1);
  };

  return (
    <div className="flex items-center gap-8">
      <IconButton
        size="sm"
        variant="outlined"
        onClick={prev}
        disabled={(lastPage > 0 && page === 1) || isLoading}
        placeholder={undefined}
        onPointerEnterCapture={undefined}
        onPointerLeaveCapture={undefined}
      >
        <ArrowLeftIcon strokeWidth={2} className="h-4 w-4" />
      </IconButton>
      <Typography
        color="gray"
        className="font-normal"
        placeholder={undefined}
        onPointerEnterCapture={undefined}
        onPointerLeaveCapture={undefined}
      >
        Page{" "}
        <strong className="text-gray-900">{lastPage > 0 ? page : 0}</strong> of{" "}
        <strong className="text-gray-900">{lastPage}</strong>
      </Typography>
      <IconButton
        size="sm"
        variant="outlined"
        onClick={next}
        disabled={(lastPage > 0 && page === lastPage) || isLoading}
        placeholder={undefined}
        onPointerEnterCapture={undefined}
        onPointerLeaveCapture={undefined}
      >
        <ArrowRightIcon strokeWidth={2} className="h-4 w-4" />
      </IconButton>
    </div>
  );
}
