import { JokeCategory } from "../interfaces/types/Joke";
import animalWebp from './../../assets/animal.webp';
import uncategorizedPng from './../../assets/uncategorized.png';
import careerPng from './../../assets/career.png';
import devWebp from './../../assets/dev.webp';
import foodWebp from './../../assets/food.webp';
import musicWebp from './../../assets/music.webp';
import moneyWebp from './../../assets/money.webp';
import movieWebp from './../../assets/movie.webp';
import travelWebp from './../../assets/travel.webp';

const allImages = [
  uncategorizedPng,
  animalWebp,
  careerPng,
  devWebp,
  foodWebp,
  musicWebp,
  moneyWebp,
  movieWebp,
  travelWebp
];

export function getImageFileGivenCategory(category: JokeCategory): string {
  const DEFAULT_CATEGORY_IMAGE = animalWebp;
  if (!(category && category.length)) {
    return DEFAULT_CATEGORY_IMAGE;
  }
  const currentCategoryImage = allImages.find(image => image.includes(category.toLowerCase()));
  return currentCategoryImage || DEFAULT_CATEGORY_IMAGE;
}