import { Joke } from "../interfaces/types/Joke";

export function getPaginatedJokes(offset: number, limit: number, allJokes?: Joke[]): Joke[] {
  if (!(allJokes && allJokes.length)) { return []; }
  const start = (offset - 1) * limit;
  const end = (start + limit) < allJokes.length ? start + limit : allJokes.length;
  const paginatedJokes = allJokes.slice(start, end);
  return paginatedJokes;

}