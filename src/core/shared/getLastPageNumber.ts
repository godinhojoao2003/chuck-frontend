import { PAGINATION_ITEMS_PER_PAGE } from "../constants";

export function getLastPageNumber(totalJokes: number): number {
  return Math.ceil(totalJokes / PAGINATION_ITEMS_PER_PAGE) || 0;
}