import React from "react";
import ReactDOM from "react-dom/client";
import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import { LoadingProvider } from "./core/context/LoadingContext.tsx";
import { JokeProvider } from "./core/context/JokeContext.tsx";
import App from "./App.tsx";
import "./global.css";

const client = new ApolloClient({
  uri: import.meta.env.VITE_API_URL,
  cache: new InMemoryCache(),
});

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <JokeProvider>
        <LoadingProvider>
          <App />
        </LoadingProvider>
      </JokeProvider>
    </ApolloProvider>
  </React.StrictMode>
);
