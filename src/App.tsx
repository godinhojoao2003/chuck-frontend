import { JokesPage } from "./pages/JokesPage.tsx";

export default function App(): JSX.Element {
  return (
    <main className="flex flex-col items-center justify-center min-h-screen p-4">
      <JokesPage />
    </main>
  );
}
