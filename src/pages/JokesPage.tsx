import { Pagination } from "./../core/components/Pagination";
import { useContext, useEffect, useState } from "react";
import debounce from "lodash.debounce";
import {
  LoadingContext,
  LoadingContextValue,
} from "./../core/context/LoadingContext.tsx";
import { JokesList } from "./../core/components/JokesList/index.tsx";
import { JokeDialog } from "./../core/components/JokeDialog/index.tsx";
import { FIND_PAGINATED_JOKES_QUERY } from "./../core/graphql/queries.ts";
import { useQuery } from "@apollo/client";
import {
  JokeContext,
  JokeContextValue,
} from "./../core/context/JokeContext.tsx";
import { Input } from "@material-tailwind/react";
import { getPaginatedJokes } from "./../core/shared/getPaginatedJokes.ts";
import { Joke } from "./../core/interfaces/types/Joke.ts";
import { getLastPageNumber } from "../core/shared/getLastPageNumber.ts";
import { PAGINATION_ITEMS_PER_PAGE } from "../core/constants.ts";

const PAGINATION_LIMIT = 1;
export function JokesPage(): JSX.Element {
  const { setIsLoading, isLoading } =
    useContext(LoadingContext) || ({} as LoadingContextValue);
  const { setTotalJokes, setAllJokes, setVisibleJokes, totalJokes, allJokes } =
    useContext(JokeContext) || ({} as JokeContextValue);
  const [page, setPage] = useState<number>(1);
  const {
    data,
    refetch,
    loading: graphqlLoading,
  } = useQuery(FIND_PAGINATED_JOKES_QUERY, {
    variables: {
      input: {
        offset: 1,
        limit: PAGINATION_LIMIT,
        searchText: "search",
      },
    },
    onCompleted: () => setIsLoading(false),
  });

  const debouncedRefetch = debounce((value: string) => {
    refetch({
      input: {
        offset: 0,
        limit: PAGINATION_LIMIT,
        searchText: value,
      },
    });
  }, 500);

  useEffect(() => {
    const responseTotalCount = data?.findPaginatedJokes?.totalCount;
    const items = data?.findPaginatedJokes?.items;
    setAllJokes(items);
    setVisibleJokes(getPaginatedJokes(1, PAGINATION_ITEMS_PER_PAGE, items));
    setTotalJokes(responseTotalCount || 0);
    setPage(responseTotalCount === 0 ? 0 : 1);
  }, [data, setAllJokes, setVisibleJokes, setTotalJokes, setPage]);

  if (graphqlLoading || isLoading) return <p>Loading...</p>;

  return (
    <>
      <div className="w-full max-w-4xl flex justify-center mb-10">
        <Input
          type="text"
          onChange={(e) => {
            if (
              e.target.value &&
              e.target.value.length >= 3 &&
              e.target.value.length <= 120
            ) {
              debouncedRefetch(e.target.value || "search");
            }
          }}
          placeholder="Type here..."
          label="Search Jokes (3 - 120 chars)"
          variant="outlined"
          onPointerEnterCapture={undefined}
          onPointerLeaveCapture={undefined}
          crossOrigin={undefined}
        />
      </div>

      <div className="w-full max-w-4xl flex justify-center mb-10">
        <JokesList />
      </div>
      <JokeDialog />

      <Pagination
        page={page}
        setPage={setPage}
        lastPage={getLastPageNumber(totalJokes)}
        onPageChange={(currentPage) => {
          setVisibleJokes(
            getPaginatedJokes(currentPage, PAGINATION_LIMIT, allJokes as Joke[])
          );
        }}
      />
    </>
  );
}
